:-module(specialProofs,[nonIntroduction/3, andIntroduction/3, nonElimination/3, implyIntroduction/3,
			falseElimination/3, orElimination/4]).

:-use_module(classicalAxioms).



andIntroduction(Left, Right, Proof):-
	instanceAx3(Left, Right, Formula1),
	applyMp(Left, Formula1, Formula2),
	applyMp(Right, Formula2, Formula3),
	[Formula1, Formula2, Formula3] =  Proof.
	  

/*
  Return the sequence of formulas corresponding to the proof

  Goal->K, Goal->~K |- ~Goal

  The assumptions Goal->K and Goal->~K are not included in the returned sequence

*/


nonIntroduction(Goal, K, Proof):-
	instanceAx7(Goal, K, Formula1),
	applyMp(im(Goal, K), Formula1, Formula2),
	applyMp(im(Goal, non(K)), Formula2, Formula3),
	/* Formula3 must be equal to non(Goal) */
	[Formula1, Formula2, Formula3] = Proof.

/*
  Return the sequence of formulas corresponding to the proof

  ~Goal->K, ~Goal->~K |- Goal

  The assumptions ~Goal->K and ~Goal->~K are not included in the returned sequence

*/


nonElimination(Goal, K, Proof):-
	instanceAx7(non(Goal), K, Formula1),
	applyMp(im(non(Goal), K), Formula1, Formula2),
	applyMp(im(non(Goal), non(K)), Formula2, Formula3),
	instanceAx8(Goal, Formula4),
	applyMp(Formula3, Formula4, Formula5),
	%% note: Formula5 must be equal to Goal
	Proof = [Formula1, Formula2, Formula3, Formula4, Formula5].

/*
  Return the sequence of formulas corresponding to the proof

  A |- B -> A

  The assumption A is not included in the returned sequence, thus the returned sequence is

  < A->(B->A), B->A >

*/

implyIntroduction(Assumption, Antecedent, Proof):-
	instanceAx1a(Assumption, Antecedent, Formula1),
	applyMp(Assumption, Formula1, Formula2),
	Proof = [Formula1, Formula2].

/*

  Return the sequence of formulas corresponding to the proof of

  A->C, B->C, A|B |- C.

  The assumptions are not included in the returned sequence

*/

orElimination(LeftDisjunct, RightDisjunct, Goal, Proof):-
	instanceAx6(LeftDisjunct, RightDisjunct, Goal, Formula1),
	applyMp(im(LeftDisjunct, Goal), Formula1, Formula2),
	applyMp(im(RightDisjunct, Goal), Formula2, Formula3),
	applyMp(or(LeftDisjunct, RightDisjunct), Formula3, Goal),
		
	Proof = [Formula1, Formula2, Formula3, Goal].

/*
  Build the proof p, non(p) |- q. The assumptions p and non(p) ARE NOT included in the
  returned proof.
  
*/

falseElimination(Formula, Goal, Proof):-
	instanceAx7(non(Goal), Formula, Formula1),
	instanceAx1a(Formula, non(Goal), Formula2),
	applyMp(Formula, Formula2, Formula3),
	applyMp(Formula3, Formula1, Formula4),
	instanceAx1a(non(Formula), non(Goal), Formula5),
	applyMp(non(Formula), Formula5, Formula6),
	applyMp(Formula6, Formula4, Formula7),
	instanceAx8(Goal, Formula8),
	applyMp(non(non(Goal)), Formula8, Goal),
	
	Proof = [Formula1, Formula2, Formula3, Formula4, Formula5, Formula6, Formula7,
	Formula8, Goal].
	
	 
test:-
	falseElimination(p, q, P),
	prettyPrint([p, non(p) | P], [p, non(p)], [], 1).
