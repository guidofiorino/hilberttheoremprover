:-module(deductionTheorem, [dischargeAssumption/4]).

:-use_module(classicalAxioms).



/*

  Given the proof InProof, DischargingAssumption is discharged. Assumptions is the set of
  assumptions of InProof that remains untouched in Result. The relative order of the lines
  in Inproof is preserved.  The formula in the last line of Result is
  DischargingAssumption->H, where H is the formula occurring in the last line of InProof.
  
  Note that some assumptions of InProof could not occurr in the Resulting proof.
  


*/

dischargeAssumption(InProof, Assumptions, DischargingAssumption, Result):-
	dischargeAssumptionHelper(InProof, Assumptions, DischargingAssumption, [],
	OutProof),

	/*
	  Check that che conclusion of OutProof is of the kind DischargingAssumption->H,
	  where H is the conclusion of InProof

	*/

	last(InProof, InProofConclusion),
	last(OutProof, OutProofConclusion),

	(
	 OutProofConclusion = im(DischargingAssumption, InProofConclusion),
	 !,
	 Result = OutProof
	;
	 
	 InProofConclusion = OutProofConclusion,
	 !,
	 Line1 = im(OutProofConclusion, im(DischargingAssumption, OutProofConclusion)),
	 Line2 = im(DischargingAssumption, OutProofConclusion),
	 append(OutProof, [Line1, Line2], Result)
	;

	 /*
	   Case Star: If we are here InProofConclusion has not been rewritten because it
	   appears in OutProof. To meet the post conditions, we return a proof concluding
	   DischargingAssumption->InProofConclusion
	 */

	 Line = im(DischargingAssumption, InProofConclusion),
	 append(OutProof, [Line], Result)
	).
	

/*
  Base case: no proof line to fix.

*/

dischargeAssumptionHelper([], _, _, FixedProof, FixedProof).

/*
  Fix line H. To do this we need the already FixedProof
*/

dischargeAssumptionHelper([ H | T], Assumptions, DischargingAssumption, FixedProof,
			  ProofResult):-

	deductionTheorem(H, Assumptions, DischargingAssumption, FixedProof,
			 FixedProofForH),
	
	dischargeAssumptionHelper(T, Assumptions, DischargingAssumption,
				  FixedProofForH, ProofResult).
	
/*

  The proof-line is the assumption to be discharged
  
*/

deductionTheorem(DischAss, _, DischAss, FixedProof, ProofLines):-
	!,
	Ax1a = im(DischAss, im(DischAss, DischAss)),
	Line1 = Ax1a,
	
	Ax1b = im(DischAss, im(im(DischAss, DischAss), DischAss)),
	Line2 = Ax1b,

	Ax1b2 = im(Ax1a, im(Ax1b, im(DischAss, DischAss))),
	Line3 = Ax1b2,

	Line4 = im(Ax1b, im(DischAss, DischAss)),
	Line5 = im(DischAss, DischAss), 
	append(FixedProof, [Line1, Line2, Line3, Line4, Line5], ProofLines).
		     
	
/*

  The proof-line is an assumpition different to the one to be discharged or an axiom: we
  do not do any change.

*/

deductionTheorem(Formula, Assumptions, _, FixedProof, ProofLines):-
	(
	 memberchk(Formula, Assumptions)
	;
	 isAxiom(Formula, _)
	),
	!,
	append(FixedProof, [Formula], ProofLines).

/*

  Formula is the conclusion of a modus ponens application:

             Left  Left->Formula  
             -------------
                 Formula
  
*/

deductionTheorem(Formula, _, DisAss, FixedProof, ProofLines ):-
	
	(
	 /* case 1: Left and Left-> Formula are in FixedProof: nothing need to be done */  

	 member(im(Left, Formula), FixedProof),
	 member(Left, FixedProof),
	 !,
	 append(FixedProof, [Formula], ProofLines)
	;
	 
	 /*
	   If we are here at least one between Left and Left->Formula is missing.
	   
	   Case 2: DisAss->Formula is an axiom: we insert it in the new proof
	 */
	 
	 Target = im(DisAss, Formula),
	 isAxiom(Target, _),
	 !,
	 append(FixedProof, [Target], ProofLines)
	;
	 /*
	   If we are here at least one between Left and Left->Formula is missing.
	   
	   Case 3: DisAss->Formula is in FixedProof: nothing to do
	   
	   Note that if Formula is the cinclusion of the input proof, then we are erasing
	   the conclusion of the input proof and we do not respect the post condition that
	   guarantees that the last formula of the returned proof is DA -> CONCLUSION.  To
	   fix the things appropriately we have Case Star in predicate dischargeAssumption
	   
	 */
	 
	 Target = im(DisAss, Formula),
	 memberchk(Target, FixedProof),
	 !,
	 FixedProof = ProofLines
	;
	 
	 
	 /*
	   Case 4: Fixed proof contains DisAss->(Left->Formula) and DisAss->Left
	 */

	 member(im(DisAss, im(Left, Formula)), FixedProof),
	 member(im(DisAss, Left), FixedProof),
	 !,
	 LeftFormulaPrem = im(DisAss, Left),
	 RightFormulaPrem = im(DisAss, im(Left, Formula)),
	 mpRewrite(LeftFormulaPrem, RightFormulaPrem, FixedProof, ProofLines)
	
	;
	
	 /*
	   If we are here at least one between DisAss->(Left->Formula) and DisAss->Left
	   is not in FixedProof.
	   
	   Case 5: DisAss->(Left->Formula) is in FixedProof. By the previous cases it
	   follows that DisAss->Left is not FixedProof. However, we know that Left is in
	   Fixedproof. We exploit this fact to build DisAss->Left
	 */

	 member(im(DisAss, im(Left, Formula)), FixedProof),
	 member(Left, FixedProof),
	 !,
	 instanceAx1a(Left, DisAss, Line1),
	 applyMp(Left, Line1, LeftFormulaPrem),
	 append(FixedProof, [Line1, LeftFormulaPrem], NewFixedProof),
	 RightFormulaPrem = im(DisAss, im(Left, Formula)),
	 mpRewrite(LeftFormulaPrem, RightFormulaPrem, NewFixedProof, ProofLines)
	 	 
	;
	 /* case 6: DisAss->Left is in FixedProof. By the previous cases it follows that
	    DisAss->(Left->Formula) is not in FixedProof. However, we know that
	    Left->Formula is in FixedProof. We exploit this fact to build
	    DisAss->(Left->Formula)
	 */
	 member(im(DisAss, Left), FixedProof),
	 member(im(Left, Formula), FixedProof),
	 !,
	 instanceAx1a(im(Left, Formula), DisAss, Line1),
	 applyMp(im(Left, Formula), Line1, RightFormulaPrem),
	 append(FixedProof, [Line1, RightFormulaPrem], NewFixedProof),
	 LeftFormulaPrem = im(DisAss, Left),
	 mpRewrite(LeftFormulaPrem, RightFormulaPrem, NewFixedProof, ProofLines)
	;
	 writeln('Error in deductionTheorem. We abort'),
	 abort
	).



/* We assume that LeftPremise and RightPremise are in FixedProof */
mpRewrite(LeftPremise, RightPremise, FixedProof, ProofLines):-
	LeftPremise = im(Da, X),
	RightPremise = im(Da, im(X, Formula)),
	instanceAx1b(Da, X, Formula, Line1),
	applyMp(LeftPremise, Line1, Line2),
	applyMp(RightPremise, Line2, Line3),
	append(FixedProof, [Line1, Line2, Line3], ProofLines).

test1:-
	Formula = p,
	Assumptions = [],
	DisAss = Formula,
	FixedProof = [],
	deductionTheorem(Formula, Assumptions, DisAss, FixedProof, ProofLines ),
	prettyPrint(ProofLines, Assumptions, [], 1).

test2:-
	Formula = p,
	Assumptions = [p],
	DisAss = q,
	FixedProof = [],
	deductionTheorem(Formula, Assumptions, DisAss, FixedProof, ProofLines ),
	prettyPrint(ProofLines, Assumptions, [], 1).

test3:-
	InProof = [a, im(a,b), b],
	Assumptions = [im(a,b)],
	DischargingAssumption = a,
	writeln('Input Proof:'),
	prettyPrint(InProof, [DischargingAssumption | Assumptions], [], 1),
	dischargeAssumption(InProof, Assumptions, DischargingAssumption, Result),
	writeln('Output Proof:'),
	prettyPrint(Result, Assumptions, [], 1).

test4:-
	InProof = [a, im(a,b), b],
	Assumptions = [a],
	DischargingAssumption = im(a,b),
	writeln('Input Proof:'),
	prettyPrint(InProof, [DischargingAssumption | Assumptions], [], 1),
	dischargeAssumption(InProof, Assumptions, DischargingAssumption, Result),
	writeln('Output Proof:'),
	prettyPrint(Result, Assumptions, [], 1).

