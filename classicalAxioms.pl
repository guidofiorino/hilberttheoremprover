:-module(axioms,[isAxiom/2, printproof/1, applyMp/3, instanceAx7/3, instanceAx1a/3,
		instanceAx1b/4, instanceAx8/2, instanceAx4a/3, instanceAx4b/3,
		instanceAx5a/2, instanceAx3/3, instanceAx5a/2, instanceAx5b/2,
		instanceAx6/4, isHilbertProof/2, deduceFromContradiction/3, getAtoms/2,
		goOnByContradictionExtendedCondition/2, prettyPrint/4, prettyProof/5,
		beautyProof/4, deduceContradictoryConjunction/3,
		addContradictoryConjunction/3, eraseUnusedLines/2]).



instanceAx6(A, B, C, Result):-
	Im1 = im(A, C),
	Im2 = im(B, C),
	Im3 = im(or(A, B), C),
	Result = im(Im1, im(Im2, Im3)).

instanceAx5a(or(A, B), im(A, or(A, B))).
instanceAx5b(or(A, B), im(B, or(A, B))).



instanceAx3(A, B, Result):-
	Result = im(A, im(B, and(A, B))).
	  
instanceAx4a(A, B, Result):-
	Result = im(and(A, B), A).

instanceAx4b(A, B, Result):-
	Result = im(and(A, B), B).


instanceAx8(A, Result):-
	Result = im(non(non(A)), A). 

instanceAx1a(A, B, Result):-
	Result = im(A, im(B, A)).

instanceAx1b(A, B, C, Result):-
	X = im(A, B),
	Y = im(A, im(B, C)),
	Z = im(A, C),
	Result = im(X, im(Y, Z)).
	
instanceAx7(A, B, Result):-
	!,
	Y = im(A, B),
	Z = im(A, non(B)),
	Result = im(Y, im(Z, non(A))).


applyMp(X, Y, Z):-
	Y = im(X,Z).

/*
  isAxiom(axiom, name)
*/

isAxiom(im(A, im(_, A)), 'Ax 1a'):-
	!.

isAxiom(X, 'Ax 1b'):-
	Y = im(A, B),
	Z = im(A, im(B, C)),
	K = im(A, C),
	X = im(Y, im(Z, K)),
	!.

isAxiom(im(A, im(B, and(A, B))), 'Ax 3'):-
	!.

isAxiom(im(and(A, _), A), 'Ax 4a'):-
	!.

isAxiom(im(and(_, B), B), 'Ax 4b'):-
	!.

isAxiom(im(A, or(A, _)), 'Ax 5a'):-
	!.

isAxiom(im(B, or(_, B)), 'Ax 5b'):-
	!.

isAxiom(X, 'Ax 6'):-
	Y = im(A, C),
	Z = im(B, C),
	K = im(or(A, B), C),
	X = im(Y, im(Z, K)),
	!.

isAxiom(X, 'Ax 7'):-
	Y = im(A, B),
	Z = im(A, non(B)),
	X = im(Y, im(Z, non(A))),
	!.

isAxiom(im(non(non(A)), A), 'Ax 8').


printproof([]):-
        writeln('end proof.').
printproof([ H | T ]):-
        printline(H),
        printproof(T).

printline(line(Key, Formula, Type)):-
        format('~w\t ', Key),
        printFormula(Formula),
        format('  ~w~n', Type).

printFormula(X):-
	atom(X),
	!,
	write(X).

printFormula(im(X,Y)):-
	!,
	write('('),
	printFormula(X),
	write('->'),
	printFormula(Y),
	write(')').

printFormula(or(X,Y)):-
	!,
	write('('),
	printFormula(X),
	write('|'),
	printFormula(Y),
	write(')').

printFormula(and(X,Y)):-
	!,
	write('('),
	printFormula(X),
	write('&'),
	printFormula(Y),
	write(')').

printFormula(non(X)):-
	!,
	write('~'),
	printFormula(X).

printFormula(equiv(X,Y)):-
	!,
	write('('),
	printFormula(X),
	write('='),
	printFormula(Y),
	write(')').



/*

  isHilbertProof(Proof, CheckedLines) is satisfied if the reverse of CheckedLines
  concetenated with Proof is an Hilbert proof. By hypothesis, the reverse of CheckedLines
  is an Hilbert proof.

  */
  

isHilbertProof([], _):-
	!.
isHilbertProof([Line | Proof], CheckedLines):-
	Line = line(_, Formula, axiom),
	isAxiom(Formula, _),
	!,
	isHilbertProof(Proof, [Line | CheckedLines]).

isHilbertProof([Line | Proof], CheckedLines):-
	Line = line(_, _, assumption),
	!,
	isHilbertProof(Proof, [Line | CheckedLines]).

isHilbertProof([Line | Proof], CheckedLines):-
	Line = line(_, Formula, mp(LeftRef, RightRef)),
	getLabel(CheckedLines, LeftPremise, LeftRef),
	getLabel(CheckedLines, RightPremise, RightRef),
	applyMp(LeftPremise, RightPremise, Formula),
	!,
	isHilbertProof(Proof, [Line | CheckedLines]).

isHilbertProof([Line | _], CheckedLines):-
	writeln('Found error on line:'),
	printline(Line),
	writeln('\nThe checked lines are:\n'),
	printproof(CheckedLines),
	abort.

/*

  Given a proof containing two formulas X and ~X we deduce the Goal.
  Requires that last line of InProof contains one of the complementary formulas
  
*/

deduceFromContradiction(InProof, Goal, OutProof):-
	/*
	member(line(LabelTargetFormula, TargetFormula, _), InProof),
	member(line(LabelTagetNonFormula, non(TargetFormula), _), InProof),
	!,
	*/

	(
	 last(InProof, line(LabelTargetFormula, TargetFormula, _)),
	 memberchk(line(LabelTagetNonFormula, non(TargetFormula), _), InProof),
	 !
	;
	 writeln('Error in deduceFromContradiction'),
	 abort
	),
	
	
	newKey(Label1),
	instanceAx1a(TargetFormula, non(Goal), Formula1),
	Line1 = line(Label1, Formula1, axiom),

	newKey(Label2),
	instanceAx1a(non(TargetFormula), non(Goal), Formula2),
	Line2 = line(Label2, Formula2, axiom),

	newKey(Label3),
	applyMp(TargetFormula, Formula1, Formula3),
	Line3 = line(Label3, Formula3, mp(LabelTargetFormula, Label1)),

	newKey(Label4),
	applyMp(non(TargetFormula), Formula2, Formula4),
	Line4 = line(Label4, Formula4, mp(LabelTagetNonFormula, Label2)),

	newKey(Label5),
	instanceAx7(non(Goal), TargetFormula, Formula5),
	Line5 = line(Label5, Formula5, axiom),

	newKey(Label6),
	applyMp(Formula3, Formula5, Formula6),
	Line6 = line(Label6, Formula6, mp(Label3, Label5)),

	newKey(Label7),
	applyMp(Formula4, Formula6, Formula7),
	Line7 = line(Label7, Formula7, mp(Label4, Label6)),

	newKey(Label8),
	instanceAx8(Goal, Formula8),
	Line8 = line(Label8, Formula8, axiom),

	newKey(Label9),
	applyMp(Formula7, Formula8, Formula9),
	/* Note that Formula9 must be equal to Goal */
	Line9 = line(Label9, Formula9, mp(Label7, Label8)),

	append(InProof, [Line1, Line2, Line3, Line4, Line5, Line6, Line7, Line8, Line9], OutProof).


addContradictoryConjunction(InProof, TargetFormula, OutProof):-
	memberchk(line(LabelTarget, TargetFormula, TypeTarget), InProof),
	memberchk(line(LabelNonTarget, non(TargetFormula), TypeNonTarget), InProof),
	LineTarget = line(LabelTarget, TargetFormula, TypeTarget),
	LineNonTarget = line(LabelNonTarget, non(TargetFormula), TypeNonTarget),
	deduceContradictoryConjunction(InProof, [LineTarget, LineNonTarget], OutProof). 


/*

  InProof contains a contradiction and LineFormula and LineNonFormula are contradictory
  lines belonging to InProof. OutProof exends InProof with the contradictory conjunction
  from the formulas in LineNonFormula and LineFormula
  
*/

deduceContradictoryConjunction(InProof, [LineFormula, LineNonFormula], OutProof):-
	LineFormula = line(LabelTargetFormula, TargetFormula, _),
	LineNonFormula = line(LabelTagetNonFormula, non(TargetFormula), _),
	!,

	newKey(Label1),
	instanceAx3(TargetFormula, non(TargetFormula), Formula1),
	Line1 = line(Label1, Formula1, axiom),
	
	newKey(Label2),
	applyMp(TargetFormula, Formula1, Formula2),
	Line2 = line(Label2, Formula2, mp(LabelTargetFormula, Label1)),

	newKey(Label3),
	applyMp(non(TargetFormula), Formula2, Formula3),
	Line3 = line(Label3, Formula3, mp(LabelTagetNonFormula, Label2)),

	append(InProof, [Line1, Line2, Line3], OutProof).




/*

  get the atomic formulas from the failed proof attempt

*/

getAtoms([], []).
getAtoms([ Line | Proof], Atoms):-
	Line = Formula,
	(
	 atom(Formula),
	 !,
	 getAtoms(Proof, Res),
	 Atoms = [ Formula | Res ]
	;
	 getAtoms(Proof, Atoms)
	).


/*

  Implementation of line 8 of the notes: insert among the public predicates if you want to use.

  The presence of goOnByContradictionExtendedCondition or
  goOnByContradictionFirstCondition determine which procedure of the notes
  is  implemented by proofSearch
  

*/

goOnByContradictionFirstCondition(InProof, Goal):-
	\+ Goal = false,
	\+ memberchk(line(_, non(Goal), _), InProof).


/*

  Implementation of line 8a of the notes.
  
  */


goOnByContradictionExtendedCondition(InProof, Goal):-
	goOnByContradictionFirstCondition(InProof, Goal),
	(
	 %% Goal is disjunctive
	 Goal = or(_,_),
	 !
	;

	 %% Goal is atomic
	 atom(Goal),
	 !
	;

	 %% (Goal -> B)\in InProof
	 Formula = im(Goal, _),
	 memberchk(line(_, Formula, _), InProof),
	 !
	;

	 %% (B-> Goal) \in InProof
	 Formula = im(_, Goal), 
	 memberchk(line(_, Formula, _), InProof),
	 !
	;

	 %% ~(Goal | B) \in InProof
	 Formula = non(or(Goal, _)),
	 memberchk(line(_, Formula, _), InProof),
	 !
	;

	 %% ~(B | Goal) \in InProof
	 Formula = non(or(_, Goal)),
	 memberchk(line(_, Formula, _), InProof)
	;

	 %% ~(Goal & B) \in InProof
	 Formula = non(and(Goal, _)),
	 memberchk(line(_, Formula, _), InProof),
	 !
	;

	 %% ~(B & Goal) \in InProof
	 Formula = non(and(_, Goal)),
	 memberchk(line(_, Formula, _), InProof)
	;

	 %% ~(Goal -> B) \in InProof
	 Formula = non(im(Goal, _)),
	 memberchk(line(_, Formula, _), InProof),
	 !
	;

	 %% ~(B -> Goal) \in InProof
	 Formula = non(im(_, Goal)),
	 memberchk(line(_, Formula, _), InProof)
	).


/*

  Given a sequence of formulas, the set of assumptions, an empty list and a line number,
  print an annotated proof
  
*/

prettyPrint([], _, _, _).
prettyPrint([Formula | InProof], Assumptions, InTable, LineNumber):-
	(
	 isAxiom(Formula, _),
	 !,
	 Line = line(LineNumber, Formula, axiom)
	;

	 member((im(X, Formula), RightRef), InTable),
	 member((X, LeftRef), InTable),
	 !,
	 Line = line(LineNumber, Formula, mp(LeftRef, RightRef))
	;

	 memberchk(Formula, Assumptions),
	 !,
	 Line = line(LineNumber, Formula, assumption)
	;

	 writeln('Error in prettyprint'),
	 abort
	),
	printline(Line),
	NextLineNumber is LineNumber + 1,
	prettyPrint(InProof, Assumptions, [(Formula, LineNumber) | InTable], NextLineNumber).


/*

  Given a sequence of formulas, the set of assumptions, a line number and an empty list,
  return an annotated proof
  
*/

prettyProof([], _, _, InProof, InProof).
prettyProof([Formula | InProof], Assumptions, LineNumber, InProofAnn, OutProofAnn):-
	(
	 isAxiom(Formula, _),
	 !,
	 Line = line(LineNumber, Formula, axiom)
	;

	 member(line(RightRef, im(X, Formula), _), InProofAnn),
	 member(line(LeftRef, X, _), InProofAnn),
	 !,
	 Line = line(LineNumber, Formula, mp(LeftRef, RightRef))
	;

	 memberchk(Formula, Assumptions),
	 !,
	 Line = line(LineNumber, Formula, assumption)
	;

	 writeln('Error in prettyProof'),
	 abort
	),
	append(InProofAnn, [Line], NewInProofAnn),
	NextLineNumber is LineNumber + 1,
	prettyProof(InProof, Assumptions, NextLineNumber, NewInProofAnn, OutProofAnn).

	

/*

  We delete axioms, assumptions and mp applications that are not used directly or
  inderectly to get the conclusion.

  Note: since we delete formulas from \Gamma, this procedure must used with care because
  we could fall in an infinite loop.

  We use only when the proof construction is finished, that is before to prettyprint the
  proof.

*/

eraseUnusedLines(InProof, OutProof):-
	reverse(InProof, ReversedInProof),
	eraseUnusedLinesHelper(ReversedInProof, [], [], OutProof).

/*
  
  eraseUnusedLinesHelper(InProof, InUsedLines, InFixedProof, OutProof)

  InFixedProof is the proof built in the right order;

  OutProof is the returned proof in the right order.

*/


/*

  we have to distinguish the conclusion of the original proof from the
  other lines. Because of the reversing, the conclusion is the first line and it must be
  included in the intermediate and final result. We recognize we are handling the
  conclusion of the proof because in the reversing it is the first line and thus the third
  argument must be []. Thus the base case is third argument equal to []

*/

/*

  Case of first line of the original proof, where the original proof has more than one
  line that we have already handled

*/

eraseUnusedLinesHelper([H], InUsedLines, [ ProofLine | Proof], OutProof):-
	!,
	InFixedProof = [ ProofLine | Proof],
	H = line(Label, _, _),
	(
	 memberchk(Label, InUsedLines),
	 !,
	 OutProof = [ H | InFixedProof ]
	;
	 OutProof = InFixedProof
	).


/*

  Case of the last line of the original proof. This is the first line we handle and we
  must include it in the fixed proof, independently of the emptieness of T.
  
*/

eraseUnusedLinesHelper([H], _, [], OutProof):-
	!,
	OutProof = [H].



eraseUnusedLinesHelper([H  | T], _, [], OutProof):-
	!,
	H = line(_, _, Type),
	(
	 Type = mp(LeftRef, RightRef),
	 !,
	 eraseUnusedLinesHelper(T, [LeftRef, RightRef], [H], OutProof)
	 ;
	 eraseUnusedLinesHelper(T, [], [H], OutProof)
	).

/*

  We know H is not the last line of the original proof, that is it is not the first line
  we handle. We also know that is it not the first line of the original proof, that is, H
  is not the last line we handle. Thus H is a line in the middle of the proof and T is not
  empty. We have to discover if it is used by some line of the given proof, otherwise it
  is useless and we delete it

*/

eraseUnusedLinesHelper([H  | T], InUsedLines, [ProofLine | Proof], OutProof):-
	FixedProof = [ProofLine | Proof],
	H = line(Label, _, Type),
	(
	 memberchk(Label, InUsedLines),
	 !,
	 /* H is used */
	 (
	  Type = mp(LeftRef, RightRef),
	  !,
	  eraseUnusedLinesHelper(T, [LeftRef, RightRef | InUsedLines], [ H | FixedProof], OutProof)
	 ;
	  eraseUnusedLinesHelper(T, InUsedLines, [ H | FixedProof], OutProof)
	 )
	;
	 /* H is not used, we delete it */
	 eraseUnusedLinesHelper(T, InUsedLines, FixedProof, OutProof)
	).

/*

  Label is the key of the proofline containing Formula
  
*/

getLabel(Proof, Formula, Label):-
	memberchk(line(Label, Formula, _), Proof).


/*

  Given a proof annotated in Kleene style, build a proof annotated in Kleene style where
  the lines are numbered from LineNumber.

*/

beautyProof([], _, _, _).

beautyProof([Line | InProof], InTable, LineNumber, Result):-
	Line = line(Label, Formula, Type),
	(
	 Type = mp(LeftRef, RightRef),
	 !,
	 memberchk((LeftRef, LeftNumber), InTable),
	 memberchk((RightRef, RightNumber), InTable),
	 NewType = mp(LeftNumber, RightNumber)
	;
	 NewType = Type
	),
	
	NextLineNumber is LineNumber + 1,
	beautyProof(InProof, [(Label, LineNumber) | InTable], NextLineNumber, TmpResult),
	Result = [ line(LineNumber, Formula,  NewType) | TmpResult ].

