:-module(evaluations,[formulaEval/4]).

:-use_module(classicalAxioms).


/*

  formulaEval(Formula, Gamma, Goal, Value):

  use Gamma |- Goal to evaluate Formula

*/


/* If Formula occurs in the evaluation table, then return its Value */
   
formulaEval(Formula, Gamma, _, Value):-
	memberchk(Formula, Gamma),
	!,
	Value = true.

formulaEval(Formula, Gamma, _, Value):-
	memberchk(non(Formula), Gamma),
	!,
	Value = false.


formulaEval(Formula, _, Goal, Value):-
	Formula = Goal,
	!,
	Value = false.

formulaEval(Formula, _, Goal, Value):-
	non(Formula) = Goal,
	!,
	Value = true.

formulaEval(Formula, _, _, Value):-
	isAxiom(Formula, _),
	!,
	Value = true.


/*
  If we are here, then neither Formula nor non(Formula) occur in \G\vdash A.
*/

formulaEval(Formula, _, _, Value):-
	atom(Formula),
	!,
	Value = Formula.


formulaEval(Formula, Gamma, Goal, Value):-
	(
	 Formula = and(Left, Right),
	 !	 
	;
	 Formula = or(Left, Right),
	 !
	;
	 Formula = im(Left, Right)
	),
	
	formulaEval(Left, Gamma, Goal, ResLeft),
	formulaEval(Right, Gamma, Goal, ResRight),
	boolEval(Formula, ResLeft, ResRight, Value).

formulaEval(Formula, Gamma, Goal, Value):-
	Formula = non(B),
	!,
	formulaEval(B, Gamma, Goal, ResNegated),
	boolNegEval(ResNegated, Value).



test1:-
	Gamma = [],
	Goal = or(p, non(p)),
	Formula = Goal,
	formulaEval(Formula, Gamma, Goal, Result),
	writeln(Result).

test2:-
	Gamma = [],
	Goal = or(p, non(p)),
	Formula = non(Goal),
	formulaEval(Formula, Gamma, Goal, Result),
	writeln(Result).


test3:-
	Gamma = [non(or(p, non(p)))],
	Goal = false,
	Formula = non(or(p, non(p))),
	formulaEval(Formula, Gamma, Goal, Result),
	writeln(Result).


test4:-
	Gamma = [non(or(p, non(p)))],
	Goal = false,
	Formula = or(p, non(p)),
	formulaEval(Formula, Gamma, Goal, Result),
	writeln(Result).

test5:-
	Gamma = [],
	Goal = false,
	Formula = or(p, non(p)),
	formulaEval(Formula, Gamma, Goal, Result),
	writeln(Result).

test6:-
	Gamma = [non(or(p, non(p))), p],
	Goal = false,
	Formula = non(p),
	formulaEval(Formula, Gamma, Goal, Result),
	writeln(Result).

/*
  boolNegEval: negation truth table

*/

boolNegEval(true, false):-
        !.
boolNegEval(false, true):-
        !.
boolNegEval(Formula, non(Formula)).


/*

  boolEval: truth tables for binary connectives

*/

boolEval(and(_, _), ResLeft, ResRight, Result):-
        (
         ResLeft = false,
         !,
         Result = false
        ;
         ResRight =false,
         !,
         Result = false
        ;
         ResLeft = true,
         ResRight = true,
         !,
         Result = true
        ;
         Result = and(ResLeft, ResRight)
        ).

boolEval(or(_, _), ResLeft, ResRight, Result):-
        (
         ResLeft = true,
         !,
         Result = true
        ;
         ResRight = true,
         !,
         Result = true
        ;
         ResLeft = false,
         ResRight = false,
         !,
         Result = false
        ;
         Result = or(ResLeft, ResRight)
        ).

boolEval(im(_, _), ResLeft, ResRight, Result):-
        (
         ResLeft = false,
         !,
         Result = true
        ;
         ResRight = true,
         !,
         Result = true
        ;
         ResLeft = true,
         ResRight = false,
         !,
         Result = false
        ;
         Result = im(ResLeft, ResRight)
        ).

