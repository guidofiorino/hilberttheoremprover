:-module(utils, [introMp/5]).



/*

  Given InProof and a formula Target in InProof, returns Outproof, the subproof of InProof
  ending in Target

*/

subproof([Target | _], Target, [Target]):-
	!.

subproof([H | InProof], Target, [ H | Result]):-
	subproof(InProof, Target, Result).


/*

  introMp(InProof, Formula, Ref1, Ref2, OutProof)

  If Formula occurs in InProof as an assumption, then line(Label, Formula, assumption)
  occurs in InProof and introMp replaces it with line(Label, Formula, mp(Ref1,
  Ref2)).

  Otherwise InProof is left unchanged.
  
*/

introMp(InProof, Formula, Ref1, Ref2, OutProof):-
	(
	 nth0(Index, InProof, line(Label, Formula, assumption), PartProof),
	 Line = line(Label, Formula, mp(Ref1, Ref2)),
	 nth0(Index, OutProof, Line, PartProof)
	;
	 OutProof = InProof
	).

	
