:-module(basicPS, [hilbertPS/3]).
:-use_module(classicalAxioms).
:-use_module(dedThm).
:-use_module(evaluations).
:-use_module(utils).
:-use_module(specialProofs).

	
/*

  hilbertPS implements procedure HpShort of the notes.

  hilbertPS(Assumptions, Goal, Result)

  Result is either a list of formulas rapresenting an Hilbert proof whose assumptions are
  included in Assumptions or a countermodel;

    
*/


/*
  Base case: the goal is an axiom.
  
  Step 1 in HpShort.
  
*/

hilbertPS(_, Goal, Proof):-
	isAxiom(Goal, _),
	!,
	Proof = [Goal].
	
/*

  Base case: the goal is an assumption.
  
  The proof is the assumption itself.

  Step 1 in HpShort.

*/

hilbertPS(Assumptions, Goal, Proof):-
	memberchk(Goal, Assumptions),
	!,
	Proof = [Goal].
/*

  Base case: contradiction proved.

  Steps 2 and 3 in HpShort.
  
*/

hilbertPS(Assumptions, Goal, Proof):-
	member(B, Assumptions),
	member(non(B), Assumptions),
	!,
	
	(
	 Goal = false,
	 !,
	 /* Produce a proof of B and that contains ~B */
	 
	 Proof = [ non(B), B]
	;
	 
	 falseElimination(B, Goal, ProofGoal),
	 append([ non(B), B],  ProofGoal, Proof)
	).

/*

  Apply axiom 8 to double negated formulas in Assumptions

  Step 4 of HpShort
  
*/

hilbertPS(Assumptions, Goal, Proof):-
	/*
	  if non(non(B)) occurs in Assumption
	   then MidAssumptions=Assumption-non(non(B))
	   otherwise fail
	*/
	
	nth0(_, Assumptions, non(non(B)), MidAssumptions),
	!,
	NewAssumptions = [ B | MidAssumptions ],
	
	hilbertPS(NewAssumptions, Goal, RecResult),
	
	(
	 RecResult = model(_),
	 !,
	 Proof = RecResult
	;
	 /*
	   If the proof RecResult contains the assumption B, we build a proof with the
	   assumption ~~B
	 */
	 memberchk(B, RecResult),
	 !,
	 Formula1 = non(non(B)),
	 instanceAx8(B, Formula2),
	 append([Formula1, Formula2], RecResult, Proof)
	;
	 /* The assumption B is not in RecResult */
	 Proof = RecResult
	).
	
/*

  Case A & B in Assumptions, that is tableau T-and application: look for a conjunctive
  formula whose conjuncts are not formulas of the proof

  Step 5 of HpShort
  
*/

hilbertPS(Assumptions, Goal, Proof):-
	memberchk(and(L, R), Assumptions),
	!,
	subtract(Assumptions, [and(L, R)], MidAss),
	NewAssumptions = [L, R |  MidAss],
	hilbertPS(NewAssumptions, Goal, RecResult),
	(
	 RecResult = model(_),
	 !,
	 Proof= RecResult
	;
	 /* modify RecResult if it contais at least one between L or R */
	 Formula1 = and(L, R),

	 (
	  memberchk(L, RecResult),
	  !,
	  instanceAx4a(L, R, Formula2),
	  Add1 = [Formula1, Formula2]
	 ;
	  Add1 = []
	 ),

	 (
	  memberchk(R, RecResult),
	  !,
	  instanceAx4b(L, R, Formula3),
	  append(Add1, [Formula1, Formula3], Add2)
	 ;
	  Add2 = Add1
	 ),
	 list_to_set(Add2, Add3),
	 append(Add3, RecResult, Proof)
	).


/*

  New Case: extended case of modus ponens application

*/

hilbertPS(Assumptions, Goal, Proof):-
	nth0(_, Assumptions, im(X, Y), NewAssumptions),
	formulaEval(X, NewAssumptions, false, Value),
	Value = true,
	!,
	hilbertPS([X, Y | NewAssumptions], Goal, OutProof),
	(
	 OutProof = model(_),
	 !,
	 Proof = OutProof
	;
	 hilbertPS(NewAssumptions, X, ProofOfX),
	 append([ProofOfX, [im(X, Y)], OutProof], SetProof),
	 list_to_set(SetProof, Proof)
	).

/*

  Case B1 | B2 in Assumptions and Goal is false

  Step 6 of HpShort
  
*/

hilbertPS(Assumptions, Goal, Proof):-
	Goal = false,
	nth0(_, Assumptions, or(B1, B2), AssumptionsNoOr),
	!,
	
	append(AssumptionsNoOr, [B1], AssumptionsB1Ass),
	hilbertPS(AssumptionsB1Ass, false, ResultB1),
	(
	 ResultB1 = model(_),
	 !,
	 Proof = ResultB1
	;
	 \+ memberchk(B1, ResultB1),
	 !,
	 Proof = ResultB1
	;
	 append(AssumptionsNoOr, [B2], AssumptionsB2Ass),
	 hilbertPS(AssumptionsB2Ass, false, ResultB2),
	 (
	  ResultB2 = model(_),
	  !,
	  Proof = ResultB2
	 ;
	  \+ memberchk(B2, ResultB2),
	  !,
	  Proof = ResultB2
	 ;
	  /*
	    
	    By contruction ResultB1 and ResultB2 are proofs ending with a formula K_i such
	    that non(K_i) is in ResultBi. From ResultB2 we build a proof ending with K1
	    also containing non(K1). Thus we solve the problem of getting the same
	    conclusion for both. Moreover we know that B1 is assumption in ResultB1 and B2
	    is assumption in ResultB2.
	    
	  */
	  last(ResultB1, K1),
	  last(ResultB2, K2),

	  /*
	    ResultB2 concludes K2 and contains ~K2
	    ResultB1 concludes K1 and contains ~K1
	  */
	  
	  /* build proof of K2, ~K2 |- K1 */
	  falseElimination(K2, K1, ProofK1),

	  /* build proof of K2, ~K2 |- ~K1 */
	  falseElimination(K2, non(K1), ProofNonK1),

	  /* Since ResultB2 contains K2 and ~K2, B2ProofAppend is a correct proof */
	  append([ResultB2, ProofNonK1, ProofK1], B2ProofAppend),

	  /* B1ProofAppend prove false by concluding K1 and containing ~K1 */
	  ResultB1 = B1ProofAppend,

	  /*
	    Both B1ProofAppend and B2ProofAppend conclude K1 and contain ~K1.

	    Note that is not guaranteed that BiProofAppend, i\in\{1,2\} has Bi as an
	    assumption.

	  */

	  /* discharge B1 */
	  dischargeAssumption(B1ProofAppend, AssumptionsNoOr, B1, ResultB1ImNeg),

	  /* we must check that B1->~K1 is in ResultB1ImNeg */
	  (
	   memberchk(im(B1, non(K1)), ResultB1ImNeg),
	   !,
	   /* no fix is required */
	   ResultB1ImNegFixed = ResultB1ImNeg
	  ;
	   /* fix is required, build B1->~K1 from ~K1 in ResultB1ImNeg*/
	   implyIntroduction(non(K1), B1, Lines1),
	   
	   /* remove B1->K1, the conclusion of ResultB1ImNeg */
	   length(ResultB1ImNeg, Size1),
	   nth1(Size1, ResultB1ImNeg, im(B1, K1), HeadB1ImNeg),
	   !,
	   
	   /*
	     Build ResultB1ImNegFixed that concludes B1->K1 and B1->~K1 is the last but
	     one formula
	   */
	   append([HeadB1ImNeg, Lines1, [im(B1, K1)]], ResultB1ImNegFixed) 
	  ),
	  	  
	  /* discharge B2 */
	  dischargeAssumption(B2ProofAppend, AssumptionsNoOr, B2, ResultB2ImNeg),

	  /*
	    ResultB2ImNeg concludes B2->K1. We must check that B2->~K1 is in
	    ResultB2ImNeg
	  */
	  (
	   memberchk(im(B2, non(K1)), ResultB2ImNeg),
	   !,
	   /* no fix is required */
	   ResultB2ImNegFixed = ResultB2ImNeg
	  ;
	   /* fix is required, build B2->~K1 */
	   implyIntroduction(non(K1), B2, Lines2),

	   /* remove B2->K1, the conclusion of ResultB2ImNeg */
	   length(ResultB2ImNeg, Size2),
	   nth1(Size2, ResultB2ImNeg, im(B2, K1), HeadB2ImNeg),
	   !,
	   
	   /*
	     Build ResultB2ImNegFixed that concludes B2->K1 and B2->~K1 is the last but
	     one formula
	   */
	   append([HeadB2ImNeg, Lines2, [im(B2, K1)]], ResultB2ImNegFixed)
	  ),

	  /*
	    build proof: B1, B2, B1|B2 |- non(K1)
	  */
	  orElimination(B1, B2, non(K1), OrENonK1),

	  /*
	    build proof: B1, B2, B1|B2 |- K1
	  */
	  orElimination(B1, B2, K1, OrEK1),

	  /* Recall:

	     B1->K1 is the conclusion of ResultB1ImNegFixed;
	     
	     B1->~K1 is the last but one formula of ResultB1ImNegFixed;

	     B2->K1 is the conclusion of ResultB2ImNegFixed;
	     
	     B2->~K1 is the last but one formula of ResultB2ImNegFixed;

	  */

	  append(ResultB1ImNegFixed, ResultB2ImNegFixed, ListProof),
	  list_to_set(ListProof, SetProof),

	  /*
	    Proof concludes K1 and contains ~K1 in the last but four line
	  */
	  append([SetProof, [or(B1, B2)], OrENonK1, OrEK1], Proof)
	 )
	).


/*

  Case B1 | B2 in Assumptions and Goal is not false
  
*/

hilbertPS(Assumptions, Goal, Proof):-
	\+ Goal = false,
	nth0(_, Assumptions, or(B1, B2), AssumptionsNoOr),
	!,
	
	append(AssumptionsNoOr, [B1], AssumptionsB1Ass),
	hilbertPS(AssumptionsB1Ass, Goal, ResultB1),
	(
	 ResultB1 = model(_),
	 !,
	 Proof = ResultB1
	;

	 \+ memberchk(B1, ResultB1),
	 !,
	 Proof = ResultB1
	;
	 
	 append(AssumptionsNoOr, [B2], AssumptionsB2Ass),
	 hilbertPS(AssumptionsB2Ass, Goal, ResultB2),
	 (
	  ResultB2 = model(_),
	  !,
	  Proof = ResultB2
	 ;
	  \+ memberchk(B2, ResultB2),
	  !,
	  Proof = ResultB2
	 ;
	  dischargeAssumption(ResultB1, AssumptionsNoOr, B1, ResultB1ImGoal),
	  dischargeAssumption(ResultB2, AssumptionsNoOr, B2, ResultB2ImGoal),
	  orElimination(B1, B2, Goal, OrElim),

	  append(ResultB1ImGoal, ResultB2ImGoal, ListProof),
	  list_to_set(ListProof, SetProof),

	  /*
	    Since in the concatenation contains B1->Goal, B2->Goal and B1|B2, Proof is a
	    correct proof that concludes Goal
	  */
	  append([SetProof, [or(B1, B2)], OrElim], Proof)
	 )
	).

/*

  We proceed on Goal by absurd if one of the following cases holds: 

  Goal is disjunctive or a PV and the evaluation of Goal is not false

  Step 8 of HpShort
  
*/

hilbertPS(Assumptions, Goal, Proof):-
	(
	 Goal = or(_, _)
	;
	 \+ Goal = false,
	 atom(Goal)
	),
	
	formulaEval(Goal, Assumptions, false, Value),
	\+ Value = false,
	!,
	NewAssumptions = [ non(Goal) | Assumptions ],
	hilbertPS(NewAssumptions, false, RecResult),
	(
	 RecResult = model(_),
	 !,
	 Proof = RecResult
	;
	 
	 \+ memberchk(non(Goal), RecResult),
	 !,
	 /*
	   the assumption is not used, we do not need to discharge any assumption.
	   Moreover we know that the given Assumptions are inconsistent
	 */
	 
	 last(RecResult, K),

	 /*
	   RecResult contains ~K and K, the concatenation with TailProof gives a correct
	   proof ending with Goal
	 */
	 
	 falseElimination(K, Goal, GoalProof),
	 append(RecResult, GoalProof, Proof)
	
	;
	 
	 /* get the last formula of the proof */
	 last(RecResult, K),
	 
	 /* by construction we know that ~K is in RecResult */
	 dischargeAssumption(RecResult, Assumptions, non(Goal),  DischRes),
	 
	 /*
	   dischargeAssumption does not guarantee that in DischRes there is 
	   ~Goal->~K, we must check and fix if required.
	 */
	 (
	   memberchk(im(non(Goal), non(K)), DischRes),
	  !,
	  /* no fix is required */
	  OutProof = DischRes
	 ;
	  /* fix is required, build ~Goal->~K */
	  implyIntroduction(non(K), non(Goal), Lines),
	  
	  /* remove ~Goal->K, the conclusion of DischRes */
	  length(DischRes, Size1),
	  nth1(Size1, DischRes, im(non(Goal), K), Head),
	  !,
	  
	  append([Head, Lines, [im(non(Goal), K)]], OutProof) 
	 ),
	 
	 /* OutProof has ~Goal->~K and ~Goal->K and ends with ~Goal->K. Last formula of
	    TailProof is Goal */
	 nonElimination(Goal, K, TailProof),

	 /*
	   Since OurProof contains ~Goal->~K and ~Goal->K, the concatenation with
	   TailProof gives a correct proof ending with Goal
	 */
	 append(OutProof, TailProof, Proof)
	).
	


/*

  Goal is a  A & B:

  if we prove both A and B, then we concatenate the two proofs and three more lines are
  added. The two proofs can have lines in common, and in common with Assumptions. To
  avoid to have a proof with duplicated formulas, we remove them.  

  Step 9 of HpShort

*/

hilbertPS(Assumptions, Goal, Proof):-
	Goal = and(Left, Right),
	!,
	
	hilbertPS(Assumptions, Left, LeftProof),
	(
	 LeftProof = model(_),
	 !,
	 Proof = LeftProof
	;
	 hilbertPS(Assumptions, Right, RightProof),
	 (
	  RightProof = model(_),
	  !,
	  Proof = RightProof
	 ;
	  
	  andIntroduction(Left, Right, TailProof),

	  /* Join LeftProof and RightProof */
	  append(LeftProof, RightProof, AndProof),
	  list_to_set(AndProof, SetProof),

	  /*
	    since Left and Right are in LeftProof and RightProof respectively, the
	    concatenation with TailProof gives a correct proof.
	  */
	  append(SetProof, TailProof, Proof)
	  )
	).

/*

  Case Goal = B -> C

  Step 10.
  
*/

hilbertPS(Assumptions, Goal, Proof):-
	Goal = im(B, C),
	!,
	AssumptionsNew = [ B | Assumptions],
	hilbertPS(AssumptionsNew, C, MidResult),
	(
	 MidResult = model(_),
	 !,
	 Proof = MidResult
	;
	 
	 dischargeAssumption(MidResult, Assumptions, B,  Proof)
	).


/*

  Case Goal = neg B

  Step 11 of HpShort

*/

hilbertPS(Assumptions, Goal, Proof):-
	Goal = non(B),
	!,
	AssumptionsNew = [ B | Assumptions ],
	hilbertPS(AssumptionsNew, false, MidResult),
	
	(
	 MidResult = model(_),
	 !,
	 Proof = MidResult
	;
	 
	 last(MidResult, K),
	 /* K is the last formula of MidResult. By construction ~K is in MidResult also */

	 dischargeAssumption(MidResult, Assumptions, B,  DischRes),
	 /*
	   
	   B->K is the last formula in DischRes. We check if B->~K is in DischRes
	   also. Note that by our implementation of the deduction algorithm, if a formula
	   is left unchanged, then it does not depend on the discharged assumption. Thus
	   checking if B->~K is in DischRes means checking if ~K in MidResult depends on
	   the assumption B. If ~K is in DischRes, then ~K does not depends on B.
	   
	 */
	 
	 (
	  memberchk(im(B, non(K)), DischRes),
	  !,
	  /* nothing to fix because B->~K is in DischRes*/
	  OutProof = DischRes
	 ;
	  
	  /* B->~K not in DischRes, fix is required, based on ~K in DischRes*/
	  implyIntroduction(non(K), B, Lines),

	  /* remove B->K, the conclusion of DischRes */
	  length(DischRes, Size1),
	  nth1(Size1, DischRes, im(B, K), Head),
	  !,
	  
	  /*
	    Outproof concludes B->K and B->~K is the last but one formula
	  */
	  append([Head, Lines, [im(B, K)]], OutProof) 
	 ),
	 
	 /* deduce Goal = ~B from B->K and B->~K */
	 nonIntroduction(B, K, TailProof),
	 
	 /*
	   Since OutProof contains B->K and ~B->K, the concatenation with TailProof gives
	   a correct proof
	 */
	 append(OutProof, TailProof, Proof)
	).


/*

  Case Goal = B1 | B2

  Step 12
  
*/

hilbertPS(Assumptions, Goal, Proof):-
	Goal = or(_, _),
	formulaEval(Goal, Assumptions, false, Value),
	Value = false,
	!,
	(
	 firstCaseOrGoal(Assumptions, Goal, Proof),
	 !
	;
	 secondCaseOrGoal(Assumptions, Goal, Proof),
	 !
	;
	 thirdCaseOrGoal(Assumptions, Goal, Proof)
	).


/*

  Case not (B1 | B2) is in Assumptions.  Goal is an atomic formula or false.
  If Goal is an atomic formula, then ~Goal is in Assumptions.

  Remark: this step could be moved after the cases for A->B or ~A in Assumptions, so to
  give to formulas of this kind the lowest priority.

  Step 13 of HpShort
  
*/

hilbertPS(Assumptions, Goal, Proof):-
	member(non(or(B1, B2)), Assumptions),
	nth0(_, Assumptions, non(or(B1, B2)), Reduced),
	formulaEval(non(or(B1, B2)), Reduced, Goal, Value),
	\+ Value = true,
	!,
	hilbertPS(Assumptions, or(B1, B2), MidResult),
	(
	 MidResult = model(_),
	 !,
	 Proof = MidResult
	;
	 (
	  Goal = false,
	  !,
	  (
	   /* MidResult is a proof of false if it contains contains ~(B1|B2) */
	   memberchk(non(or(B1, B2)), MidResult),
	   !,
	   Proof = MidResult
	  ;
	   /* build a proof of false by using ~(B1 | B2). Note that ~(B1|B2) is in Assumptions */
	   Proof = [non(or(B1, B2)) | MidResult]
	  )
	 ;
	  
	  /* check if the assumption non(or(B1, B2)) is in MidResult */
	  (
	   memberchk(non(or(B1, B2)), MidResult),
	   !,
	   /* no fix is required: non(or(B1, B2)) is in MidResult */
	   FixProof = MidResult
	  ;
	   /* fix is required: add non(or(B1, B2)) to MidResult */
	   FixProof = [non(or(B1, B2)) | MidResult ]
	  ),
	  
	  /* build the proof for B1|B2, ~(B1|B2) |- Goal */ 
	  falseElimination(or(B1, B2), Goal, TailProof),

	  /*
	    since FixProof contains B1|B2 and ~(B1|B2), the concatenation with TailProof
	    gives a correct proof
	  */
	  append(FixProof, TailProof, Proof)
	 )
	).

/*

  Case ~B is in Assumptions, where B is not a propositional variable or a disjunction
  (Goal is an atomic formula or false).

  Loook in Assumptions for any formula ~B, such that B is a conjunction or an
  implication. Note that by previous Steps:

  1- double negated formulas are not in Assumptions, because they are handled in a step
     before;

  2- negated-disjunctive formulas can be in Assumptions. We disregard them since they are
     handled by an ad-hoc step given above.

  Step 14 of HpShort
  
*/

hilbertPS(Assumptions, Goal, Proof):-
	member(non(B), Assumptions),
	\+ atomic(B), /* disregard negated var prop */
	\+ B = or(_,_), /* and disregard negated disjunctions */
	nth0(_, Assumptions, non(B), Reduced),
	!,
	hilbertPS(Reduced, B, MidResult),
	(
	 MidResult = model(_),
	 !,
	 Proof = MidResult
	;
	 (
	  Goal = false,
	  !,
	  /* conclude the proof with a contradiction */
	  
	  Proof = [non(B) | MidResult] 
	 ;
	  
	  falseElimination(B, Goal, TailProof),
	  /* last line of Proof must be Goal */
	  append([non(B) | MidResult], TailProof, Proof)
	 )
	).

/*

  Case B -> C is in Assumptions  (Goal is an atomic formula or false)

  Step 15 of HpShort
  
*/

hilbertPS(Assumptions, Goal, Proof):-
	memberchk(im(B,C), Assumptions),
	nth0(_, Assumptions, im(B, C), Reduced),
	!,
	hilbertPS(Reduced, B, ResultOnB),
	(
	 ResultOnB = model(_),
	 !,
	 Proof = ResultOnB
	;
	 hilbertPS([C | Reduced], Goal, ResultOnC),
	 (
	  ResultOnC = model(_),
	  !,
	  Proof = ResultOnC
	 ;
	  /* if assumption C is not used, then ResultOnC prove Goal from Assumtions */
	  \+ memberchk(C, ResultOnC),
	  !,
	  Proof = ResultOnC
	 ;
	  
	  append([ResultOnB, [ im(B, C) ], ResultOnC], Proof)
	 )
	).

/*

  base case: no rule is applicable, thus the goal is not provable. We return a classical
  propositional model. The model satisfied all the formulas in Assumptions and does not
  satisfies the goal.

*/


hilbertPS(Assumptions, _, model(Atoms)):-
	getAtoms(Assumptions, Atoms). 
	



firstCaseOrGoal(Assumptions, Goal, Proof):-
	Goal = or(B1, _),
	formulaEval(B1, Assumptions, false, Value),
	\+ Value = false,
	!,
	
	hilbertPS(Assumptions, B1, MidResult),
	(
	 MidResult = model(_),
	 !,
	 Proof = MidResult
	 ;
	 
	 instanceAx5a(Goal, Formula1),
	 applyMp(B1, Formula1, Formula2),
	 append(MidResult, [Formula1, Formula2], Proof)
	 ).


secondCaseOrGoal(Assumptions, Goal, Proof):-
	Goal = or(_, B2),
	formulaEval(B2, Assumptions, false, Value),
	\+ Value = false,
	!,
	
	hilbertPS(Assumptions, B2, MidResult),
	(
	 MidResult = model(_),
	 !,
	 Proof = MidResult
	 ;
	 
	 instanceAx5b(Goal, Formula1),
	 	 
	 applyMp(B2, Formula1, Formula2),
	 append(MidResult, [Formula1, Formula2], Proof)
	).


thirdCaseOrGoal(Assumptions, Goal, Proof):-
	hilbertPS(Assumptions, false, MidResult),
	(
	 MidResult = model(_),
	 !,
	 Proof = MidResult
	;
	 /*
	   by contruction MidResult ends with a contradictory conjunction
	 */
	 
	 last(MidResult, K),
	 falseElimination(K, Goal, Res),
	 append([MidResult, Res], Proof)	 
	).


